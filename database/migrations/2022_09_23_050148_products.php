<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration {

    public function up() {

        Schema::create('products', function (Blueprint $table) {

            $table->increments('product_id')->unsigned();

            $table->string('product_name');
            $table->longText('product_description')->nullable();

            $table->enum('status', [0, 1])->default(1);
            $table->enum('publish_online', [0, 1])->default(0);
            $table->enum('featured', [0, 1])->default(0);

            $table->timestamps();

        });

    }

    public function down() {
        Schema::dropIfExists('products');
    }
}
