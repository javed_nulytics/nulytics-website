<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Model\Product;


class AppServiceProvider extends ServiceProvider {

    public function register() {

    }

    public function boot() {

        Schema::defaultStringLength(191);

        view()->composer('*', function ($view)  {

            $products = Product::where('status', '1')->get();

            $view->with('products', $products);

        });
    }
}
