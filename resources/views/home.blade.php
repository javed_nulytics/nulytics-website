@extends('layout')

@section('content')

     <!-- Top Banner -->
     <div class="banner-area">

<!-- Single Banner -->
<div class="single-banner bg-image-z fullscreen bg-parallax" data-black-overlay="5" data-velocity=".2">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-1 col-lg-12">
                <div class="single-banner-content">
                    <h1 class="font-medium cd-headline no-cursor clip text-left">
                        <span class="cd-words-wrapper">
                            <b class="is-visible">Digital Marketing</b>
                            <b>Website Development</b>
                            <b>E-commerce Solution</b>
                            <b>Mobile Apps</b>
                            <b>Hardware & IT Support</b>
                            <b>Motion Graphics</b>
                        </span>
                    </h1>
                    <p>We are {{ config('app.name') }} and we strike a harmony between creativity and code. We pride in the seamless coordination between our visual graphics team, software engineers and social media gurus. Our team members are in Boston and in Dhaka. And we are going to be with you every step of the way:
ideation → project progression → validation of your passion.
                    </p>
                    <a href="about" class="cr-btn cr-btn-lg cr-btn-round">
                        <span>More About Us</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--// Single Banner -->

</div>
<!-- //Top Banner -->

<!-- Page Content -->
<main class="page-content">



<!-- Services Area -->
<section class="services-area section-padding-lg bg-white">
    <div class="container">

        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12 offset-0">
                <div class="section-title text-center">
                    <h6>Believe In Work</h6>
                    <h2>Our Services</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">


            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="digital">
                        <img src="images/service/dmhome.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="digital">Digital Marketing</a>
                        </h4>
                        <p>Digital Marketing is a core component of an expanding business. Can your online business grow just by using a state-of-the-art expensive platform?..
                        </p>
                        <a href="digital" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="website">
                        <img src="images/service/webhome.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="website">Website Development</a>
                        </h4>
                        <p>Anybody can make websites. But your business is more than a pretty logo. Your story is uniquely yours - what inspired you to start this venture, what are your company’s vision and goals?..
                        </p>
                        <a href="website" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="ecommerce">
                        <img src="images/service/ecomhome.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="ecommerce">E-Commerce Solution</a>
                        </h4>
                        <p>You online store is open 24/7/365. Selling online to a digital consumer (B2C or B2B) requires familiarity with e-commerce best practices...
                        </p>
                        <a href="ecommerce" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="mobileapps">
                        <img src="images/service/mobileapps.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="mobileapps">Mobile Apps</a>
                        </h4>
                        <p>Use smart phone apps to leverage technology and increase client base. We have our smartphone app development team ready to support your systems and services...
                        </p>
                        <a href="mobileapps" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="hardwareit">
                        <img src="images/service/hardware.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="hardwareit">Hardware & IT Support</a>
                        </h4>
                        <p>We provide all kind of POS solution required Hardware such as Desktop Computer, Laptop, Barcode scanner, Barcode Printer, POS Printer, Laser Printer...
                        </p>
                        <a href="hardwareit" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-6 col-md-6 col-12">
                <div class="service service-style-5">
                    <div class="service-image">
                        <a href="motiongraphics">
                        <img src="images/service/motiongraphics.jpg" alt="Service Thumb">
                        </a>
                    </div>
                    <div class="service-content">
                        <h4>
                            <a href="motiongraphics">Motion Graphics</a>
                        </h4>
                        <p>Consumers like visuals. Storytelling with visuals grabs the attention of people. Get ahead of competition by using the skills of our Motion Graphics team...
                        </p>
                        <a href="motiongraphics" class="cr-readmore2">Read More</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section id="about-area" class="about-area section-padding-lg bg-grey">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-xl-5 col-lg-12 order-2 order-xl-1">
                <div class="about-content">
                    <h2>Clients Choice </h2>
                    <h4>ELECTRON POS</h4>
                    <p>Electron POS is a cloud based system that helps to manage billing, inventory tracking, human resource and payroll management, SMS marketing tools, details analytics report with integrated accounting system & more - in the form of complete business automation solutions, our technological footprint has grown to benefit of 61+ businesses across the Bangladesh.</p>
                    <a href="cupcake.html" class="cr-btn cr-btn-round cr-btn-lg">
                        <span>Know More</span>
                    </a>
                </div>
            </div>

            <div class="col-xl-7 col-lg-12 order-1 order-xl-2">
                <!-- <div class="video-box ml-0 ml-xl-5" data-tilt>
                    <div class="video-box-thumb">
                        <img src="images/video-thumbs/video-thumb-1.jpg" alt="video-thumb">
                    </div>
                    <a href="https://youtu.be/bR6ZUqXSVC4" class="play-button">
                        <i class="flaticon-play-button"></i>
                    </a>
                </div> -->
                <iframe width="560" height="315" src="https://www.youtube.com/embed/bR6ZUqXSVC4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="testimonial-area section-padding-lg bg-white">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12 offset-0">
                <div class="section-title text-center">
                    <h6>PRODUCTS OUR CLIENTS LOVE</h6>
                    <h2>Our Products</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="testimonial-wrap testimonial-style-2 sidemenu-wrapper-testimonial-slider-active cr-slider-dots-1">
                    @foreach($products as $product)
                    <div class="testimonial text-center"> 
                        <a href="">
                        <div class="testimonial-thumb"><img src="images/icons/cupcake.png" alt="{{ $product->product_name }}"></div>

                        <h5>{{ $product->product_name }}</h5>
                        <div class="testimonial-content"><p>{{ $product->product_description }}</p></div>
                        <div class="testimonial-author"><p><a href="">Learn More..</a></p></div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</section>

<section class="callto-action-area bg-theme">
<div class="container">
    <div class="row">
        <div class="col-xl-10 offset-xl-1 col-12 offset-0">
            <div class="callto-action">
                <div class="callto-action-inner">
                    <h2>Work with our amazing team!</h2>
                    <a href="contact" class="cr-btn cr-btn-white">
                        <span>Get In Touch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

</main>

@endsection