@extends('layout')


@section('content')

    <div class="breadcrumb-area-products" data-black-overlay="7">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1">
                    <div class="cr-breadcrumb text-center">
                        <h1>Products</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <main class="page-content">

        <section class="services-area section-padding-lg bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-12 offset-0">
                        <div class="section-title text-center">

                            <h2>Available Products</h2>
                            <p>Have a look at our product collection. We make the systems our clients love!</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">

                    @foreach($products as $product)
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="service service-style-2 text-center">
                                <div class="service-icon">
                                    <span><img src="images/icons/cupcake.png" alt="{{ $product->product_name }}"></span>
                                    <span><i class="bi bi-search"></i></span>
                                </div>
                                <div class="service-content">
                                    <h4>{{ $product->product_name }}</h4>
                                    <p>{{ $product->product_description }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>

        @include('partials.call-to-action')

    </main>

@endsection